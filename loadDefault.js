#!/usr/bin/env node

var origProjectDir = __dirname + "/origProject";

var server = require("./server");

exports.setUp = function(callback) {
    server.start(8080, function() {
        callback();
    });
};

var accepted_files = ['gif','jpg', 'png'];
var re = /(?:\.([^.]+))?$/; //regex for file extension

var dir = origProjectDir + "/images/thumbnails";
var fs = require('fs');
var files = fs.readdirSync(dir);

for(var i in files) {
	extension = re.exec(files[i])[1]; //get file extension
    if (accepted_files.indexOf(extension) > -1) {
		var id = files[i].match(/^\d+\.?\d*/g);
        if(id !== null){
            id=id[0];
            console.log("Found %s: - id:%s",files[i],id);

            var request = require('supertest');
            var express = require('express');
            var request = require('superagent');
            var nocache = require('no-cache');
            var app = express();

            var thumb = "/images/thumbnails/" + id + '-m.jpg';
            var large = "/images/large/" + id + '-b.jpg';

            var Template = require('./app/models/template');
            var newTemplate = new Template();
                newTemplate.thumbnail = thumb;
                newTemplate.id = id;
                newTemplate.large = large;
                newTemplate.title =  "Template #" + id;
                newTemplate.description =  "This is a description for template #" + id;
                newTemplate.cost =  id + " Million Dollars!";

            var settingsClass = require("./tests/settings");
            testSettings = new settingsClass();

            var url = '/save/';
            request
                .post(testSettings.server + url)
                .send(newTemplate)
                .set('Accept', 'application/json')
                .end(function(err, res){
                    if(res!==undefined){
                        if(res.error!==false){
                            console.log(res.error);
                        }else{
                            console.log("added " + id);
                        }
                    }else{
                        console.log(err);
                        console.log(url +" result is undefined");
                    }
                });

                //copy over files
                
                console.log("- copying "+ thumb);
                fs.createReadStream(origProjectDir + thumb).pipe(fs.createWriteStream(__dirname + "/public/" + thumb));
                
                console.log("- copying "+ large);
                fs.createReadStream(origProjectDir + large).pipe(fs.createWriteStream(__dirname + "/public/" + large));
                

        }
        
	}
}
console.log("ctrl+c to finish");

