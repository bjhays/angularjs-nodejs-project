#!/usr/bin/env node
try {
    var reporter = require('nodeunit').reporters.default;
}
catch(e) {
    console.log("Cannot find nodeunit module.");
    console.log("You can download submodules for this project by doing:");
    console.log("");
    console.log("    git submodule init");
    console.log("    git submodule update");
    console.log("");
    process.exit();
}

// var server = require("./server");

// exports.setUp = function(callback) {
//     server.start(8080, function() {
//         callback();
//     });
// };

var dir = "tests";
var fs = require('fs');
var files = fs.readdirSync(dir);
process.chdir(dir);
for(var i in files) {
	if(files[i]!="run.js" && files[i]!="settings.js"){
		console.log("Running tests in %s:",files[i]);
		reporter.run([files[i]]);
	}
}