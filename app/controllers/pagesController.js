var locomotive = require('locomotive'), Controller = locomotive.Controller;

var pagesController = new Controller();

pagesController.main = function() {
	this.title = 'Code Development Test';
	this.header = 'Code Development Project';
	this.render();
};

module.exports = pagesController;


