var locomotive = require('locomotive'), Controller = locomotive.Controller;

var editController = new Controller();

editController.edit = function() {
	this.title = 'Edit Templates Code Development Test';
	this.header = 'Edit Templates Code Development Project';
	if(this.req.session.error !== undefined){
		console.log("message %s",this.req.session.error);
		this.messages = this.req.session.error;
		if(this.req.session.error!==""){
			this.req.session.error = ""; //clear messages after use
		}
	}else{
		this.messages = "";
	}

   this.render();
};

editController.deleteTemplate = function(){
	//remove files
	var fs = require('fs');
	var helpers = require('../../lib/helpers');
	helpers = new helpers();
	var imagePath = __dirname + "/../../public/";
	var xmlPath = __dirname + "/../../data/templates/";

	var POST = this.request.body;
	var messages = "";

	//thumbnail
	var file = imagePath + POST.thumbnail;
	result = helpers.deleteFile(file);
	messages += result.messages;
	
	//large
	file = imagePath + POST.large;
	result = helpers.deleteFile(file);
	messages += result.messages;
	
	//xml
	file = xmlPath + POST.id + ".xml";
	result = helpers.deleteFile(file);
	messages += result.messages;

	var ret = {message:messages + "\n"};
	return this.res.json(ret);
};

editController.save = function() {
	var fs = require('fs');
	var express = require('express');
    var xml2js = require('xml2js');
	var builder = new xml2js.Builder();

	var thumbTargetPath = "thumbnails";
	var largeTargetPath = "large";
	var accepted_files = ['gif','jpg', 'png'];
	var re = /(?:\.([^.]+))?$/; //regex for file extension

	var thumbnailImage = "";
	var largeImage = "";
	var messages = "";
	var POST = this.request.body;

	// 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
    if (this.request.body.length > 1e6) {
        // FLOOD ATTACK OR FAULTY CLIENT, NUKE REQUEST
        messages+= "FLOOD ATTACK OR FAULTY CLIENT" + "\n";
        this.request.connection.destroy();
    }

    var xmlFile = __dirname + "/../../data/templates/" + POST.id + ".xml";
    
    var xmlExists = fs.existsSync(xmlFile);
    if (!xmlExists || POST.edit==1) {
		if(this.req.files!== undefined){
			if(this.req.files.thumbnail.size>15728640){
				messages += "File too large" + "\n";
			}else{
				var thumbnail=this.req.files.thumbnail;
				if(thumbnail.originalFilename!==""){
					extension = re.exec(thumbnail.originalFilename)[1]; //get file extension
					if (accepted_files.indexOf(extension) > -1) {
						thumbnailImage = "images/" + thumbTargetPath + "/" + thumbnail.originalFilename;
						fs.readFile(thumbnail.path, function (err, data) {
							var newPath = __dirname + "/../../public/" + thumbnailImage;
							fs.writeFile(newPath, data, function (err) {
								if (err) return console.log(err);
							});
						});
					}else{
						thumbnailImage="";
						messages+= thumbnail.originalFilename + " is not an accepted file type" + "\n";
					}
				}
			}


			if(this.req.files.thumbnail.size>15728640){
				messages += "File too large" + "\n";
			}else{
				var large=this.req.files.large;
				if(large.originalFilename!==""){
					extension = re.exec(large.originalFilename)[1]; //get file extension
					if (accepted_files.indexOf(extension) > -1) {
						largeImage = "images/" + largeTargetPath + "/" + large.originalFilename;
						fs.readFile(large.path, function (err, data) {
							var newPath = __dirname + "/../../public/" + largeImage;
							fs.writeFile(newPath, data, function (err) {
								if (err) return console.log(err);
							});
						});
					}else{
						largeImage="";
						messages += large.originalFilename + " is not an accepted file type" + "\n";
					}
				}
			}
		}
		var Template = require('../models/template');
		var newTemplate = new Template();
			newTemplate.thumbnail = (thumbnailImage==="")?POST.thumbnail:thumbnailImage;
			newTemplate.id = POST.id;
			newTemplate.large = (largeImage==="")?POST.large:largeImage;
			newTemplate.title =  POST.title;
			newTemplate.description =  POST.description;
			newTemplate.cost =  POST.cost;

		
		var xml = builder.buildObject(newTemplate);

		fs.writeFileSync(xmlFile, xml);

		this.req.session.error = messages;
		this.res.redirect("../edit/?t="+POST.id);
	}else{
		messages += "Could not create duplicate template #"+ POST.id;
		this.req.session.error = messages;
		var templateRet = "";
		Object.keys(POST).forEach(function(key) {
			var val = POST[key];
			templateRet += "&"+key+"="+encodeURIComponent(val);
		});
		this.res.redirect("../edit/?t=new"+templateRet);
	}
};

module.exports = editController;


