var locomotive = require('locomotive'), Controller = locomotive.Controller;

var templatesController = new Controller();

templatesController.show = function(){
	var accepted_files = ['xml'];
	var perpage = this.param('perpage');
	if(perpage === undefined){ perpage=4;}
	
	var fs = require('fs');
	var imagePath="images/";
	var thumbnailDir="thumbnails";
	var fullImageDir="large";
	var templateInfoPath="data/templates";
	
	var templates = {};
	var page=[];
	var templateCount=0;
	var files = fs.readdirSync(templateInfoPath);
	var lastPage=0;
	for(var i in files) {
		var curfile = files[i];
		var re = /(?:\.([^.]+))?$/; //regex for file extension
		var extension = re.exec(curfile)[1]; //get file extension
		
		if (accepted_files.indexOf(extension) > -1) {
			templateCount++;
		
			var xml2js = require('xml2js');
			var parser = new xml2js.Parser();
			var xml = fs.readFileSync(templateInfoPath + '/' + curfile,'utf8');
			
			parser.parseString(xml, function (err, result) {
				var templateFile=result.root;
			
				var Template = require('../models/template');
				var newTemplate = new Template();
					newTemplate.thumbnail = (typeof templateFile.thumbnail !== 'undefined')?templateFile.thumbnail[0]:"";
					newTemplate.id = (typeof templateFile.id !== 'undefined')?templateFile.id[0]:"";
					newTemplate.large = (typeof templateFile.large !== 'undefined')?templateFile.large[0]:"";
					newTemplate.title =  (typeof templateFile.title !== 'undefined')?templateFile.title[0]:"";
					newTemplate.description =  (typeof templateFile.description !== 'undefined')?templateFile.description[0]:"";
					newTemplate.cost =  (typeof templateFile.cost !== 'undefined')?templateFile.cost[0]:"";
					newTemplate.result = i;
				page.push(newTemplate);
			});
		}
		if((templateCount%perpage===0 || i==(files.length-1)) && (i>0 || (templateCount===1 && page.length>0))){
			if(perpage==0){
				pageNum=1;
			}else{
				pageNum=Math.ceil(templateCount/perpage);
			}
			lastPage=pageNum;
			templates[pageNum]=page;
			page=[];
		}
	}

	var ret = {count:templateCount,pages:lastPage,templates:templates};
	return this.res.json(ret);
};

module.exports = templatesController;


