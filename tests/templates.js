/*exports.testSomething = function(test){
    test.expect(1);
    test.ok(true, "this assertion should pass");
    test.done();
};

exports.testSomethingElse = function(test){
    test.ok(false, "this assertion should fail");
    test.done();
};
*/

exports.testTemplateJson = function(test){
	var request = require('supertest');
	var express = require('express');
	var settingsClass = require("./settings");
	testSettings = new settingsClass();
	var app = express();

	var url = '/templates/0';
	try{
		request(testSettings.server)
			.get(url)
			.expect('Content-Type', /json/)
			.expect(200)
			.end(function(err, res){
				test.notEqual(res,undefined);
				if(res!==undefined){
					test.equal(res.error,false);
					if(res.error!==false){
						console.log(res.error);
					}
				}else{
					console.log(err);
					console.log(url +" result is undefined");
				}
				
				test.done();
				
		});
	}catch(e){
		console.log("Error" + e);
		test.done();
	}
};

exports.testAddTemplate = function(test){
	var request = require('supertest');
	var express = require('express');
	var request = require('superagent');
	var nocache = require('no-cache');
	var app = express();

	var Template = require('../app/models/template');
	var newTemplate = new Template();
		newTemplate.thumbnail = "";
		newTemplate.id = "1234567890";
		newTemplate.large = "";
		newTemplate.title =  "Test Template";
		newTemplate.description =  "This is a test template";
		newTemplate.cost =  "1 Million Dollars!";

	var settingsClass = require("./settings");
	testSettings = new settingsClass();

	var url = '/save/';
	request
		.post(testSettings.server + url)
		.send(newTemplate)
		.set('Accept', 'application/json')
		.end(function(err, res){
			test.notEqual(res,undefined);
				if(res!==undefined){
					test.equal(res.error,false);
					if(res.error!==false){
						console.log(res.error);
					}
				}else{
					console.log(err);
					console.log(url +" result is undefined");
				}
				
				test.done();
		});
};

exports.testDeleteTemplate = function(test){
	/*
	data: {'id':template.id,
                      'thumbnail': template.thumbnail,
                      'large' : template.large
                      },
	*/
	var request = require('supertest');
	var express = require('express');
	var request = require('superagent');
	var nocache = require('no-cache');
	var app = express();
	var settingsClass = require("./settings");
	testSettings = new settingsClass();
	
	var url = '/edit/delete/';
	request
		.post(testSettings.server + url)
		.send({ id: '1234567890', thumbnail: '',large: '' })
		.set('X-API-Key', 'foobar')
		.set('Accept', 'application/json')
		.end(function(err, res){
			test.notEqual(res,undefined);
				if(res!==undefined){
					test.equal(res.error,false);
					if(res.error!==false){
						console.log(res.error);
					}
				}else{
					console.log(err);
					console.log(url +" result is undefined");
				}
				
				test.done();
		});
};