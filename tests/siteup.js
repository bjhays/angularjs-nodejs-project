exports.testPing = function(test){
	var request = require('supertest');
	var express = require('express');
	var settingsClass = require("./settings");
	testSettings = new settingsClass();
	var app = express();

	var url = '/ping';
	try{
		request(testSettings.server)
			.get(url)
			.expect('Content-Type', /text/)
			.expect(200)
			.end(function(err, res){
				test.notEqual(res,undefined);
				if(res!==undefined){
					test.equal(res.error,false);
					if(res.error!==false){
						console.log(res.error);
					}
				}else{
					console.log(err);
					console.log(url +" result is undefined");
				}
				
				test.done();
				
		});
	}catch(e){
		console.log("Error" + e);
		test.done();
	}
};

exports.testMainController = function(test){
	var request = require('supertest');
	var express = require('express');
	var settingsClass = require("./settings");
	testSettings = new settingsClass();
	var app = express();

	var url = '/';
	try{
		request(testSettings.server)
			.get(url)
			.expect('Content-Type', /text/)
			.expect(200)
			.end(function(err, res){
				test.notEqual(res,undefined);
				if(res!==undefined){
					test.equal(res.error,false);
					if(res.error!==false){
						console.log(res.error);
					}
				}else{
					console.log(err);
					console.log(url +" result is undefined");
				}
				
				test.done();
				
		});
	}catch(e){
		console.log("Error" + e);
		test.done();
	}
};

exports.testEditController = function(test){
	var request = require('supertest');
	var express = require('express');
	var settingsClass = require("./settings");
	testSettings = new settingsClass();
	var app = express();

	var url = '/edit/';
	try{
		request(testSettings.server)
			.get(url)
			.expect('Content-Type', /text/)
			.expect(200)
			.end(function(err, res){
				test.notEqual(res,undefined);
				if(res!==undefined){
					test.equal(res.error,false);
					if(res.error!==false){
						console.log(res.error);
					}
				}else{
					console.log(err);
					console.log(url +" result is undefined");
				}
				
				test.done();
				
		});
	}catch(e){
		console.log("Error" + e);
		test.done();
	}
};