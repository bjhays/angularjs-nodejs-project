function helpers()
{
    
}

// class methods
helpers.prototype.deleteFile = function(file) {
	var fs = require('fs');
	var messages = "";
	var success = null;

	if(file.slice(-1)==="/"){
		return {success:true,messages:"invalid file"};
	}
	try{
		fs.exists(file, function(exists) {
			if (exists) {
				fs.unlinkSync(file);
				success = true;
				messages += 'successfully deleted ' + file + "\n";
			} else {
				messages += file + "Does not exist" + "\n";
				success = true; //although we didn't delete it is not there == goal!
			}
			console.log("Deleted " + file + ": "+ messages);
			return {success:success,messages:messages};
		});
	}catch (e){
		console.log(e);
		success = false;
		messages += "Error deleting " + file + ":" + e.stingify() + "\n";
	}
	console.log("Deleting " + file + ": "+ messages);
	return {success:success,messages:messages};
};
// export the class
module.exports = helpers;