// Draw routes.  Locomotive's router provides expressive syntax for drawing
// routes, including support for resourceful routes, namespaces, and nesting.
// MVC routes can be mapped to controllers using convenient
// `controller#action` shorthand.  Standard middleware in the form of
// `function(req, res, next)` is also fully supported.  Consult the Locomotive
// Guide on [routing](http://locomotivejs.org/guide/routing.html) for additional
// information.
module.exports = function routes() {
  //main page
  this.root('pages#main');
  //json for templates
  this.match('templates/', { controller: 'templates', action: 'show' });
  this.match('templates/:perpage', { controller: 'templates', action: 'show' });
  //edit templates
  this.match('edit/', { controller: 'edit', action: 'edit' });
  this.match('edit/:template', { controller: 'edit', action: 'edit' });
  
  //delete template end point, post
  this.post('edit/delete/', 'edit#deleteTemplate');
  //save template end point, post
  this.post('save/', 'edit#save');

  this.get('/ping', function(req, res){
      res.send("pong!", 200);
  });
};
