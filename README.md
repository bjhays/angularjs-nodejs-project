Technical Requirements
-----------------------------
Develop the project as a MVC application using object oriented programming. You can use any MVC framework, but don’t feel compelled to use a particular mvc framework. No framework is required. Just build the project using MVC paradigms and if you are stuck on MVC, just use good OO principles. The HTML and CSS has been provided for you in the files \index.html and \style\style.css
￼
Project Requirements and Steps
------------------------------
A completed project view is below in pdf

1. Display the thumbnail images of templates in a filmstrip view. The template images are located in the \images\thumbnails directory.
2. Set the number of thumbnails to view as 4 and implement a “Next” link to advance the “window” of up to 4 thumbnail images through the entire set of thumbnail images. For example, if there are 15 thumbnail images total, the initial view would show 4 thumbnails, then clicking “Next” would advance to the next 4 until the final 3 are visible in which case the “Next” link is no longer visible.
3. Display previous and next links to move a “sliding window” of 4 thumbnail images through the entire list of website template thumbnail images.. For example, with a total of 15 thumbnail images, this would mean that initially thumbnail images 1-4 appear with a previous link and a next link. Clicking next would display images 5-8 or clicking previous would display images 12-15.
4. Display the template id under the thumbnail image as shown above. The template id is the numerical part of the thumbnail filename.
5. Add the ability to click on a thumbnail image and make the currently selected thumbnail image appear with a border (css styles enable this), it is ok to invoke a page refresh to do this.
6. Include the ability to have the large version of the thumbnail image, located in \images\large appear when that thumbnail is selected. If there is no currently selected thumbnail, the previously selected thumbnail image should remain in the larger viewer window when clicking on the next or previous links. On initial load, the first large image in the thumbnails sliding window should be visible.

## Additional capability if time permits:
Add functionality to add a website template, edit the website template, and delete a website template. The template information should persist to an XML file.
You should include the following fields in the add / edit form
Title
Description
ID#
Cost
Thumbnail File Large Image File
The title, description, ID#, and cost should be displayed along with the larger image when the thumbnail image is selected.

To run
------------------------------
## Prerequisites
1. Install node (https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager)
2. run "npm install locomotive -g" to install locomotive
3. unit tests require nodeunit ("npm install nodeunit -g")
npm install passport
npm install passport-google

## Main functionality
1. run "lcm server" from terminal in the project directory
3. navigate to http://localhost:3000/ in browser

## Edit/Add functionality
1. run "lcm server" from terminal in the project directory
2. navigate to http://localhost:3000/edit in browser

## Unit Tests
1. ensure that runTests.js is executable
2. run ./runTests.js from the project directory

## Load Defaults and Run
1. ensure that loadDefaults.js is executable
2. run ./loadDefaults.js from the project directory


Assumptions
-------------------------------
* the only way new templates will be added is through /edit view
* ID is unique to a template and numeric

Notes
--------------------------------
Style guide used: http://nodeguide.com/style.html