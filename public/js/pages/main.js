var app = angular.module('templatesApp', []);
var numTemplatesPerPage=4;

app.factory('templateFactory', ['$http', function ($http){
    var factory = {};
    factory.getImages = function() {
        return $http.get('templates/');
    };
    return factory;
}]);

app.controller('templateController', function($scope,$http,templateFactory){

    function init(){
        $scope.pagedTemplates = {};
        templateFactory.getImages().success(function(data){
            if(data.count>0){
                $scope.totalTemplates=data.count;
                $scope.nextPage=1;
                $scope.lastPage=Math.ceil($scope.totalTemplates/numTemplatesPerPage);
                $scope.images = data.templates;

                angular.forEach($scope.images, function(value, key){
                   for (var i = 0; i < value.length; i++) {
                        value[i].isSelected = false;
                    }
                 });

                $scope.totalPages=data.pages;
                $scope.pagedTemplates = $scope.images[1];
                $scope.curTemplate = $scope.pagedTemplates[0];
                $scope.setTemplate($scope.pagedTemplates[0]);
            }else{
                $scope.mainImageUrl="images/No-Image-Available.gif";
            }
        });
    }


     $scope.showPagedTemplates= function(page) {
        $scope.pagedTemplates = $scope.images[page];
        $scope.nextPage=(page+1>$scope.totalPages)?1:page+1;
        $scope.lastPage=(page-1<1)?$scope.totalPages:page-1;
        if($scope.nextPage===1){
            $scope.LastPage=true;
        }else{
            $scope.LastPage=false;
        }
    };

     $scope.setTemplate = function(template) {
        $scope.curTemplate.isSelected = false;
        $scope.curTemplate = template;
        $scope.mainImageUrl = template.large;
        $scope.mainTitle = template.title;
        $scope.mainDescription = template.description;
        $scope.mainCost = template.cost;
        $scope.mainId = template.id;
        $scope.mainThumb = template.thumbnail;
        $scope.mainLarge = template.large;
        template.isSelected = true;
    };

    init();
});