var app = angular.module('templatesApp', [],function($httpProvider) {
  // Use x-www-form-urlencoded Content-Type
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
 
  /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   */
  var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;
      
    for(name in obj) {
      value = obj[name];
        
      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }
      
    return query.length ? query.substr(0, query.length - 1) : query;
  };
 
  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];
})
    .config(['$locationProvider',
        function($locationProvider) {
            $locationProvider.html5Mode(true);
    }]);

app.factory('xmlFactory', ['$http', function ($http){
    var factory = {};
    factory.getTemplates = function() {
        return $http.get('../templates/0');
    };
    return factory;
}]);

app.controller('EditController', function($scope,$http,xmlFactory, $location){

    function init(){
        //get parameter and load template to edit if set
        $scope.editId=$location.search().t;

        $scope.Templates = {};
        xmlFactory.getTemplates().success(function(data){
          if(data.count>0){
            $scope.Templates = data.templates[1];
            if($scope.editId=== undefined || $scope.getTemplateById($scope.editId)=== undefined){
                $scope.templateSelected=0;
            }else{
                $scope.templateSelected=$scope.getTemplateById($scope.editId).result;
            }

            if($scope.editId=="new"){
                $scope.result = 0;
                $scope.title = $location.search().title;
                $scope.description = $location.search().description;
                $scope.cost = $location.search().cost;
                $scope.id = $location.search().id;
                $scope.mainThumb = $location.search().thumbnail;
                $scope.mainLarge = $location.search().large;
                $scope.editTemplate = 0; //really a new template
            }else{
              $scope.editTemplate($scope.Templates[$scope.templateSelected]);
            }
          }
        });
    }

    $scope.editTemplate = function(template) {
        $scope.response = "";
        $scope.edit = false;
        $scope.result = template.result;
        $scope.title = template.title;
        $scope.description = template.description;
        $scope.cost = template.cost;
        $scope.id = template.id;
        $scope.mainThumb = template.thumbnail;
        $scope.mainLarge = template.large;
        $scope.editTemplate = 1;
    };

    $scope.newTemplate = function() {
        $scope.response = "";
        $scope.edit = true;
        $scope.title = "";
        $scope.description = "";
        $scope.cost = "";
        $scope.id = "";
        $scope.mainThumb = "";
        $scope.mainLarge = "";
        $scope.editTemplate = 0;
    };

    $scope.getTemplateById = function(id){
        for (var i = 0; i < $scope.Templates.length; i++) {
            if($scope.Templates[i].id==id){
                return $scope.Templates[i];
            }
        }
    };

    $scope.deleteTemplate = function(result){
        var template = $scope.Templates[result];
        if(template !== undefined){
          $http({
              method: 'POST',
              url: '../edit/delete/',
              data: {'id':template.id,
                      'thumbnail': template.thumbnail,
                      'large' : template.large
                      },
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).success(function(response) {
            console.log(response);
              $scope.response = response.message;
              $scope.loaded = true;
                  $scope.Templates.splice( $scope.Templates.indexOf(template), 1 );
                  $scope.newTemplate();
          });
        }else{
          $scope.response = "Error: please refresh and try again";
        }
    };

    init();
});